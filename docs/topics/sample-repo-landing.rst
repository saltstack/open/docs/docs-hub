:hide-toc:
:sd_hide_title:


.. div:: landing-title

    .. grid::
        :reverse:
        :gutter: 2 3 3 3
        :margin: 4 4 1 2

        .. grid-item::
            :columns: 12 3 3 3
            :class: sd-m-auto sd-animate-grow50-rot20

            .. image:: ../_static/img/repo-landing-small.png
              :width: 125
              :alt: Salt Project repo logo

        .. grid-item::
            :columns: 12 9 9 9
            :class: sd-text-white sd-fs-2 sd-text-center

            The Salt Project Repository

            .. button-link:: https://repo.saltproject.io/py3/
               :color: light
               :align: center
               :outline:

                Browse the directory of package files



=================
Salt Project repo
=================

Install Salt
============

.. grid:: 3

    .. grid-item::

        .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/
           :color: primary
           :expand:

           Salt install guide :octicon:`arrow-up-right`

    .. grid-item::

        .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/topics/bootstrap.html
           :color: primary
           :expand:

           Bootstrap install :octicon:`arrow-up-right`

    .. grid-item::

        .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/topics/salt-supported-operating-systems.html
           :color: primary
           :expand:

           Supported systems :octicon:`arrow-up-right`

Quick install by operating system
=================================

Operating systems are listed in alphabetical order:

.. grid:: 3
    :gutter: 3

    .. grid-item-card:: Amazon Linux 2
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Amazon Linux 2 <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/amazon.html#install-salt-on-amazon-linux-2>`

    .. grid-item-card:: Arch Linux
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Arch Linux install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/arch.html#install-salt-on-arch-linux>`

    .. grid-item-card:: CentOS
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Centos Stream 9 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-salt-on-centos-stream-9>`
       :bdg-link-primary:`Centos Stream 8 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-salt-on-centos-stream-8>`
       :bdg-link-primary:`Centos 7 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-salt-on-centos-7>`

    .. grid-item-card:: Debian
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Debian 11 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html#install-salt-on-debian-11-bullseye>`
       :bdg-link-primary:`Debian 10 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html#install-salt-on-debian-10-buster>`

    .. grid-item-card:: Fedora
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Fedora install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/fedora.html#install-salt-on-fedora>`

    .. grid-item-card:: FreeBSD
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`FreeBSD install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/freebsd.html#install-salt-on-freebsd>`

    .. grid-item-card:: macOS
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`macOS install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/macos.html>`

    .. grid-item-card:: Oracle Linux
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Oracle Linux install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/oracle.html>`

    .. grid-item-card:: Photon OS
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Photon OS 4 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/photonos.html#install-salt-on-photon-os-4>`
       :bdg-link-primary:`Photon OS 3 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/photonos.html#install-salt-on-photon-os-3>`

    .. grid-item-card:: RedHat (RHEL)
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`RHEL 9 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-salt-on-redhat-rhel-9>`
       :bdg-link-primary:`RHEL 8 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-salt-on-redhat-rhel-8>`
       :bdg-link-primary:`RHEL 7 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-salt-on-redhat-rhel-7>`

    .. grid-item-card:: SUSE (SLES)
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`SUSE install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/sles.html#install-salt-on-suse>`

    .. grid-item-card:: Ubuntu
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Ubuntu 22.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-salt-on-ubuntu-22-04-jammy>`
       :bdg-link-primary:`Ubuntu 20.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-salt-on-ubuntu-20-04-focal>`

    .. grid-item-card:: Windows
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Windows install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/windows.html>`

    .. grid-item-card:: Directory listing
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Bootstrap <https://repo.saltproject.io/bootstrap/>`
       :bdg-link-primary:`OSX <https://repo.saltproject.io/osx/>`
       :bdg-link-primary:`py3 <https://repo.saltproject.io/py3/>`
       :bdg-link-primary:`Release artifacts <https://repo.saltproject.io/release-artifacts/>`
       :bdg-link-primary:`salt/ <https://repo.saltproject.io/salt/>`
       :bdg-link-primary:`salt_rc/ <https://repo.saltproject.io/salt_rc/>`
       :bdg-link-primary:`salt-dev/ <https://repo.saltproject.io/salt-dev/>`
       :bdg-link-primary:`salt-singlebin/ <https://repo.saltproject.io/salt-singlebin/>`
       :bdg-link-primary:`windows <https://repo.saltproject.io/windows/>`
