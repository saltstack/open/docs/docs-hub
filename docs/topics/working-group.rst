.. _working-group:

=======================
Salt docs working group
=======================

Come talk to the Salt documentation working group by joining the
``#documentation`` channel on the
`Salt Community Slack <https://saltstackcommunity.slack.com>`_.

The current working group captain is Alyssa Rock:

* `Alyssa's GitHub profile <https://github.com/barbaricyawps>`_
* `Alyssa's GitLab profile <https://gitlab.com/barbaricyawps>`_

To find out what the docs team is currently working on, look at the
`Salt Docs Roadmap <https://saltdocs.prodcamp.com/>`_.


Purpose of the Salt docs working group
======================================
The purpose of the Salt documentation working group is to improve the overall
quality of Salt documentation by:

* Decreasing the time investment for new users to onboard
* Scaling the docs for various user skill levels, beginning to advanced
* Optimizing navigation for speed and relevance
* Enhancing real world applicability by adding tutorials and clear use-case
  scenarios
* Providing clear indicators of module maturity and stability in related
  documentation

You should consider contributing to the docs for many reasons:

* **Improving open source documentation has a big impact.** Good documentation
  helps orient newcomers, improves the project’s overall usability, and ensures
  that the community continues to grow. Even small contributions make a big
  difference.
* **Users at any skill level can contribute.** If you are new to Salt and want
  to contribute as a community member, the Documentation Working Group is one of
  the best groups to join because it has a low barrier to entry for
  contribution. You will get more familiar with Salt and what it is like to
  contribute while working on a low-stress project appreciated by the entire
  community. If you are a seasoned user, your expertise is valuable because you
  have a better sense of what users expect when reading software documentation.
  All contributions are welcome!
* **Documentation makes a difference.** In 2017, GitHub conducted the
  `Open Source Survey <https://opensourcesurvey.org/2017/>`_, sampling 5,500
  respondents. One key takeaway was "documentation is highly valued, but often
  overlooked." The survey found outdated or confusing documentation to be a
  major problem, affecting 93% of respondents. Yet 60% of contributors rarely or
  never contribute to documentation. Consider giving back to the community by
  helping improve the docs.


Contributing to the Salt documentation
======================================
You can contribute to the Salt documentation in many ways. Going from easiest to
most difficult, you can:

1. `Open an issue to report doc bugs or provide doc feedback`_.

2. `Request or upvote a major doc initiative`_.

3. Open up a pull request to fix the docs yourself. See the
   `Salt Contributing guide <https://github.com/saltstack/salt/blob/master/CONTRIBUTING.rst>`_
   for more info.

4. `Join the Salt Docs working group`_.


Open an issue to report doc bugs or provide doc feedback
--------------------------------------------------------
The best way to provide doc feedback or to report bugs is to open an issue.
In general, we try to review all new tickets within 1-2 weeks. See
:ref:`docs-triage-process` for more information.

To request a doc improvement or report a bug in the documentation, open an issue
in the repository where you noticed the bug. In most cases, that's the ``salt``
repository:

1. Go to the **Issues** tab in the ``salt`` repository in GitHub:
   https://github.com/saltstack/salt/issues

2. Click **New issue** and select the **Get started** button next to **Docs** or
   **Bug report**.

3. Fill in the template.

   .. Note::
       See `Creating an issue - GitHub Docs <https://docs.github.com/en/issues/tracking-your-work-with-issues/creating-an-issue>`_
       for more info.

When providing doc feedback, keep in mind that it's easier for our team to take
action if you suggest specifically *how* you want the documentation to be fixed:

.. list-table::
  :widths: 50 50
  :header-rows: 1

  * - Okay feedback
    - "These docs really suck! Fix them!"

  * - Much better feedback
    - "This command has a typo and produces an error message. The correct
      command should be..."

No matter if your documentation request is big or small, we'll consider it
all! Even if you don't have time to personally contribute to the docs, just
providing feedback is a valuable contribution in and of itself. But always keep
in mind that fixing doc issues yourself is WAY more helpful and is strongly
encouraged!


Request or upvote a major doc initiative
----------------------------------------
If you want to help the Docs working group decide which major initiative they
should work on next, you can either request or upvote an initiative by checking
out the `Salt Docs Roadmap <https://saltdocs.prodcamp.com/>`_. This roadmap
lists major initiatives (epics) that the Docs team is considering working on.
You can login and add your own request, make a comment, or upvote an initiative.

While we can't guarantee that the initiative with the most votes will be the
first thing we work on, we will take every piece of feedback very seriously and
use it to guide our decisions.


Join the Salt Docs working group
--------------------------------
Docs working group meetings take place every other Tuesday at 4p.m. Mountain
Time / 3p.m. Pacific Time / 23:00 UTC. Check the Community Calendar for the
Zoom link.

Guidelines for contributing to (and communicating with) this working group:

* It is not required to be a member to assist the group's goal achievement.
  However, only members may advocate for solutions.

* Action is emphasized throughout the lifecycle and not simply planning and
  deliberation.

* Please complete task commitments. Do not over commit to task beyond your means.

* Ask for help and communicate to avoid developing in a vacuum. We encourage
  asking for help from other group members and from the Salt community at large.

* Put good faith effort into resolving issues with the current toolset before
  requesting to add/replace a tool.
