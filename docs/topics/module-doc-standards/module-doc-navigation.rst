================================================
How to find the right Salt module for your needs
================================================

This document will be a guide to navigating and reading the module
documentation. We basically need some sort of guide that explains how to
find the right Salt module and then how to read the Salt module documentation.

We could make the guide for reading the Salt module docs a separate guide?
