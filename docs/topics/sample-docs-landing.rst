:hide-toc:
:sd_hide_title:


.. div:: landing-title

    .. grid::
        :reverse:
        :gutter: 2 3 3 3
        :margin: 4 4 1 2

        .. grid-item::
            :columns: 12 3 3 3
            :class: sd-m-auto sd-animate-grow50-rot20

            .. image:: ../_static/img/repo-landing-small.png
              :width: 125
              :alt: Salt Project docs landing logo

        .. grid-item::
            :columns: 12 9 9 9
            :class: sd-text-white sd-fs-2 sd-text-center

            The Salt Project Docs

            .. button-link:: https://docs.saltproject.io/en/latest/py-modindex.html
               :color: light
               :align: center
               :outline:

                Go to the Salt module docs



=================
Salt Project docs
=================


Currently supported Salt releases
=================================

.. grid:: 3

    .. grid-item-card::
       :class-card: sd-border-1
       :shadow: md

       .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/
          :color: success
          :expand:

          Salt 3005.0

    .. grid-item-card::
       :class-card: sd-border-1
       :shadow: md

       .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/
          :color: primary
          :expand:

          Salt 3004.2

    .. grid-item-card::
       :class-card: sd-border-1
       :shadow: md

       .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/
          :color: info
          :expand:

          3003.5
