.. _personas:

==================
Salt user personas
==================

The Salt user personas are based on extensive user research conducted by the
Salt Docs working group. See
`Salt community listening tour report <https://docs.google.com/presentation/d/1YFOVSpnqMZxEQmOTrMkoI-Sk1P7BVyz22FNP3ee86cU/edit?usp=sharing>`_ for more
information about the research methodology.

.. Tip::
    If you're unfamiliar with user personas, see `What are UX personas and what are they used for? <https://www.uxdesigninstitute.com/blog/what-are-ux-personas/>`_ from the UX Design Institute.

Salt has five main user personas:

* `Sys Admin Sam`_
* `DevOps Architect Ari`_
* `Enterprise Erin`_
* `Prospective User Pat`_
* `Community Contributor Cas`_


Sys Admin Sam
=============

.. sidebar:: **Sys Admin Sam**

    .. figure:: /_static/img/sam.png
       :align: right

       "Help me easily install, deploy, and maintain business critical software and hardware."


Likely a junior or mid-level systems administrator. Approaches Salt from an Operations, systems administration, and IT perspective.


Skills
------

- **Salt:** Beginner to intermediate
- **Operations:** Intermediate to advanced
- **Development:** Beginner to intermediate


Goals
-----

- Learn how Salt can help them spend less time on the stressful or boring and repetitive parts of system administration and focus more on building an efficient, secure, and self-healing automated systems.
- Identify how Salt can provide solutions for their biggest time sinkholes and bottlenecks, such as system installation and upgrades, software deployment, and preventing or quickly remediating system outages.
- Aspires to grow professionally so they can eventually have the same skills and proficiencies with Salt and systems administration as DevOps Architect Ari.


Values
------

- Using Salt to turn "pets" (highly customized machines) into "cattle" (generic machines with standardized deployments and configuration that can scale).
- Playbook of features and common use cases at disposal for streamlining day-to-day tasks and problem-solving processes.
- Trustworthy, recommended, and tested best practices to ensure the reliability and security of systems.


Pain points
-----------

- Needs more guidance around the common tasks and workflows that most sys admins use Salt for.
- Struggles to bridge the gap toward stateful management and infrastructure-as-code best practices. For example, they may have frustrations over losing manual changes when to Salt overwrites system configurations that were implemented manually to solve a break/fix.
- Limited time to dive deeply and research which of the many possible solutions Salt provides will be the best approach.
- Uncertainty around Salt's feature set or what it lacks, especially in comparison to similar tools.


Use cases
---------

- Configure, deploy, and maintain hardware and software to maintain business critical operations.
- Monitor and maintain networks on-premises and in cloud environments.
- Automate the new deployments for quick and consistent system configurations.
- Troubleshoot issues with installations, updates, patches, and service failures.
- Write states to define and maintain desired configurations, ensuring a secure and reliable IT environment.
- Implement security measures and adhere to compliance standards.


Common competencies
-------------------

- Windows systems administration skills.
- Linux/Unix administration skills.
- Ability to write and use shell scripts.
- Expertise in networking protocols for configuring network settings, firewalls, and connectivity.



DevOps Architect Ari
====================

.. sidebar:: **DevOps Architect Ari**

    .. figure:: /_static/img/ari.png
       :align: right

       "I want to build a more efficient, cost-saving system."


Likely a mid-level or senior developer or DevOps architect. Approaches Salt from a DevOps and development perspective.


Skills
------

- **Salt:** Intermediate to advanced
- **Operations:** Intermediate to advanced
- **Development:** Intermediate to advanced


Goals
-----

- Build stateful, automated infrastructure solutions that meets the core business needs and long-term business objectives.
- Design, build, and run IT systems, services, and infrastructure to meet changing business requirements.
- Hopes to discover new ways that Salt can help optimize their system architecture and processes by learning what other solutions have been developed and tested in the Salt Project community and beyond.
- Wants confidence that Salt solutions are battle-tested and verified in real-world deployments at scale and in harmony with industry best practices.
- Needs to quickly and easily train stakeholders (such as team members, clients, or coworkers in other departments) so that they can understand Salt, start working with it quickly, and/or understand how it impacts their work.


Values
------

- The expertise provided by the Salt Project community.
- Wants multiple, in-depth examples and case studies that offer insights into real-world scenarios. Examples need enough context to provide a deeper understanding of how Salt works.
- Ability to customize solutions to specific needs and opportunities to explore edge cases.
- Understand the relationship between the different architectural components, modules, and features of Salt to learn how to develop solutions using Salt. This includes information that can help them evaluate related enterprise products if it makes sense for their business needs.
- Understanding the nuanced relationship between various Salt elements, including enterprise and open-source components.


Pain points
-----------

- Appreciates that Salt provides many different ways to do something, but struggles to know which solution is the best or the easiest or the most scalable. The hard part is weighing how much time it will take to discover a solution, learn it in depth, and see how it interacts in their environment. That initial upfront research cost is very high.
- Difficulty navigating the module documentation to discover the solutions they need.
- Problems interacting with a code base where the underlying data dictionary-based makes it challenging to discover information programmatically.
- Locating and retrieving buried solutions, examples, and use cases from the community forums and Slack channels.


Use cases
---------

- Writing states and orchestrations to define and manage the desired configurations of systems effectively.
- Scaling infrastructures efficiently to provision and configure machines to meet growing business needs.
- Implementing automation and orchestration to streamline repetitive tasks, cut costs, and enhance operational efficiency.
- Managing continuous software deployment, ensuring efficient and consistent updates across infrastructures and a reliable delivery pipeline.
- Optimizing system configurations for better performance, security, and to align with industry best practices.


Common competencies
-------------------

- Linux/Unix systems administration skills.
- Windows systems administration skills.
- Python and possibly additional languages (C#, Ruby, Pearl, and more).
- DevOps and infrastructure-as-code best practices.
- Infrastucture-as-Code tools like Terraform or CloudFormation.
- Containerization tools like Docker or Kubernetes.
- Version control tools like Git.
- Templating and configuration skills like YAML/Jinja.
- Command line interfaces.


Enterprise Erin
===============

.. sidebar:: **Enterprise Erin**

    .. figure:: /_static/img/erin.png
       :align: right

       "I want to use Salt in a large enterprise environment."


Uses the enterprise products built on Salt, likely in a large-scale enterprise that could be in a highly regulated industry such as health care, finance, and the government. Approaches Salt from a Windows systems administration and enterprise perspective.


Skills
------

- **Salt:** Beginner to intermediate
- **Operations:** Intermediate to advanced
- **Development:** Beginner to intermediate


Goals
-----

- Needs to understand enough about Salt in order to use the enterprise product effectively in a large-scale, potentially highly regulated enterprise environment.
- Wants access to a community of experts who can provide advice and insights about Salt best practices and answer occasional technical support questions.
- Needs assurance that modules developed by the Salt community are secure, technically sound, bug-free, and in harmony with current industry best practices.
- Hopes for a simplified process for establishing stability and availability for all systems and networks.
- Wants to integrate the enterprise tool with their organization's existing systems and tools while also complying with business requirements and regulations.
- Demonstrate to stakeholders that the system is compliant with security standards, industry best practices, and existing regulations.


Values
------

- Likes using with GUI-based systems that work out-of-the-box instead of working with command-line interfaces, especially because it reduces the cost of onboarding new coworkers and stakeholders.
- Appreciates having dedicated support available that can quickly and confidently resolve any issues or questions that may arise.
- Predictable, reliable updates and consistency in desktop and software configurations across all systems, reducing variability and enhancing operational stability.
- More flexibility (especially compared to Microsoft Systems Center), allowing custom configurations and automation to meet specific needs and requirements.
- Wants to see that other major companies and organizations trust Salt and its related enterprise products to solve large, enterprise-scale problems.


Pain points
-----------

- It is difficult to discover the relationship between Salt and its enterprise product offerings. The enterprise product documentation and the open source documentation are fragmented from each other and need to be better linked. It is difficult to discover the Salt community and discover what a great resource they are for working with Salt.
- Salt is easier for users with backgrounds in Linux, CLI systems, and/or Python. Enterprise users may or may not have any experience with these technologies. Lack of familiarity with Linux or Python can prevent users from taking full advantage of Salt and its enterprise counterpart.
- Lack of readily available resources and support tailored to Windows-based environments.
- The full Salt capabilities can be overwhelming, requiring dedicated time and effort to discover and understand.
- Properly formatting commands according to precise syntax within the command-line interface (CLI) is a challenge and leads to errors and inefficiencies.
- Using Salt in an air-gapped environment.
- Setting up Salt and its enterprise counterpart for high availability and disaster recovery.


Use cases
---------

- Streamlining server builds and using Salt automation to swiftly provision and configure virtual machines.
- Improving efficiency for software installations, verifying that the correct software versions and configurations are consistently applied to all target systems.
- Identifying and addressing potential issues before they escalate through log management and monitoring.
- Automating security scans and compliance checks to ensure that systems adhere to security policies and standards. Using the power of Salt to immediately remediate security scans and infrastructure that is out of compliance.


Common competencies
-------------------

- In-depth knowledge of Windows systems administration.
- Knowledge about the specific regulatory requirements for their industry.
- Expertise in networking protocols for configuring network settings, firewalls, and connectivity.
- Familiarity with PowerShell scripting.



Prospective User Pat
====================

.. sidebar:: **Prospective User Pat**

    .. figure:: /_static/img/pat.png
       :align: right

       "I need you to show me how Salt could be better than our existing tech stack."


Likely has experience with another automation and configuration management framework such as Ansible, Chef, or Puppet. Approaches Salt from the perspective of a new user with prior knowledge of other tools.


Skills
------

- **Salt:** Beginner
- **Operations:** Beginner to advanced
- **Development:** Beginner to advanced


Goals
-----

- Discover how Salt can solve their organization's specific challenges and pain points.
- Compare Salt to the tool they are already familiar with, identifying strengths and weaknesses of Salt compared to Ansible, Chef, or Puppet.
- Learn how they can apply and extend their knowledge from their previous configuration management tools to Salt.
- Make informed decisions and optimize IT infrastructure management processes.
- Learn about how the Salt framework for automation and orchestration differs from their previous experience.


Values
------

- Flexibility for efficient infrastructure scaling and seamless adaptation to evolving business needs.
- Advanced orchestration capabilities to manage complex workflows and ensure efficient task execution.
- Quicker and more customizable automation to streamline repetitive tasks and configurations with greater speed and precision.
- Remote execution for workflows, enabling team members to manage systems from a distance effectively.
- The ability to easily migrate or use their existing workflows in Salt with minimal disruption to services.
- Ease of learning Salt and onboarding additional teams and stakeholders.
- Keeping costs and effort low.


Pain points
-----------

- Steeper learning curve when transitioning to Salt compared to Ansible in the early stages of adoption.
- Translating Ansible knowledge to Salt concepts, especially concerning the use of agents.
- More components to secure compared to Ansible's agentless architecture.
- Absence of backward compatibility, potentially impacting the ability to seamlessly integrate existing infrastructure with Salt.


Use cases
---------

- Creating a score card comparing Salt to similar technologies based on business requirements.
- Making a business case for one technology or another for a key stakeholder or decision maker.


Common competencies
-------------------

- Windows systems administration skills.
- Linux/Unix administration skills.
- Ability to write and use shell scripts.
- Expertise in networking protocols for configuring network settings, firewalls, and connectivity.
- Python and possibly additional languages (C#, Ruby, Pearl, and more).
- Containerization tools like Docker or Kubernetes.
- Version control tools like Git.
- Command line interfaces.


Community Contributor Cas
=========================

.. sidebar:: **Community Contributor Cas**

    .. figure:: /_static/img/cas.png
       :align: right

       "I want to give back to Salt to make it better for both my org and others."


Likely a Salt power user or someone looking to contribute to a major open source project. Generally approaches Salt from more of a DevOps or development perspective.


Skills
------

- **Salt:** Intermediate to advanced
- **Operations:** Beginner to advanced
- **Development:** Beginner to advanced


Goals
-----

- Cares about Salt and wants to see it succeed.
- Exchanging ideas, news, and best practices with other members of the Salt Project community.
- Enjoys networking and developing new skills professionally.
- Wants to mentor others and collaborate.


Values
------

- Clear communication from Salt Project, the core development team, and its corporate sponsors.
- Timely responses to new issues and pull requests.
- Exchanging ideas, news, and best practices with other members of the Salt Project community.
- Having a voice and an active say in key decisions about Salt.
- Opportunities to meet other Salt users and developers.


Pain points
-----------

- Understanding the contributing process, such as setting up a development environment and writing elegant code and effective tests.
- Keeping up with the standards and requirements set by the core Salt development team.
- Worries about the maintainability and future sustainability of Salt.


Use cases
---------

- Creating new Salt extensions and maintaining existing ones.
- Testing new release candidates.
- Finding bugs and opening issues.
- Joining and participating in Salt community events, such as Open Hour and working groups.


Common competencies
-------------------

- Windows systems administration skills.
- Linux/Unix administration skills.
- Ability to write and use shell scripts.
- Python and possibly additional languages (C#, Ruby, Pearl, and more).
- Version control tools like Git.
- Command line interfaces.
