.. _tech-stack:

====================
Salt docs tech stack
====================

Salt is a Python-based tool. For that reason, the Salt docs tech stack consists
of documentation tools that have been developed primarily by the Python
development community. Most tools are open source.

The tech stack consists of the following:

.. list-table::
  :widths: 20 45 35
  :header-rows: 1
  :stub-columns: 1

  * - Tool name
    - Purpose
    - For more information

  * - Furo
    -  * Documentation theme.
       * Applies CSS styling to the documentation's HTML.
       * Gives the overall visual look and feel + branding.
    -  * `Furo docs <https://pradyunsg.me/furo/>`_
       * `Furo repo <https://github.com/pradyunsg/furo>`_
       * Additionally, consider joining the
         `Write the Docs Slack Workspace <https://www.writethedocs.org/slack/>`_
         where you can talk to the maintainer of this theme and other
         users who use this theme on the ``#sphinx`` channel.

  * - Git
    -  * Version control.
       * Version control allows you track changes to a project (code and
         documentation) over time. You can rollback changes if needed.
    -  Git is such an important technology that many vendors offer resources.

  * - GitLab
    -  * Hosts the documentation code and content.
       * CI/CD (continuous integration/continuous deployment). CI/CD tools check
         that the code or documentation contribution passes additional quality
         tests and can be deployed to a live environment. They ensure that test
         environments match live environments.
    -  * `GitLab docs <https://docs.gitlab.com/>`_

  * - GitLab Pages
    -  * Hosts the public-facing web version of our documentation content.
       * CI/CD (continuous integration/continuous deployment). CI/CD tools check
         that the code or documentation contribution passes additional quality
         tests and can be deployed to a live environment. They ensure that test
         environments match live environments.
       * DNS settings are handled by the SRE team.
    -  * `GitLab Pages docs <https://docs.gitlab.com/ee/user/project/pages/>`_

  * - reStructured Text (rst)
    -  * Document formatting and source code.
       * Provides formatting elements to display the documentation correctly
         (e.g. headings, lists, tables, links, tabs, etc.).
    -  * :ref:`rst-guide`

  * - Nox
    -  * Build your docs locally with Sphinx to get a local environment preview.
       * Also acts as an initial quality check on the documentation prior to
         CI/CD integration.
       * Customized with the ``noxfile.py`` file.
    -  * `Nox docs <https://nox.thea.codes/en/stable/>`_
       * These docs aren't always helpful. It's sometimes best just to ask Pedro
         or Wayne on the core Salt team for help with Nox. Salt is kind of an
         early adopter of Nox and Pedro is their champion on the team.

  * - Pre-commit
    -  * A framework for pre-commit hooks.
       * Basically, it's a utility that runs in your local Git environment and
         as part of the GitLab CI/CD pipeline when you open an upstream merge
         request. It checks that you’re committing quality code or docs before
         you can commit them. If your changes fail pre-commit, you’ll need to
         fix them and run it again.
       * Most of the time it works well, but it sometimes gives false warnings
         that you need to tell it to ignore or override.
       * Pre-commit is triggered by the ``git commit`` command locally.
    -  * `Pre-commit repos <https://github.com/pre-commit/>`_
       * `Pre-commit hooks you must know <https://towardsdatascience.com/pre-commit-hooks-you-must-know-ff247f5feb7e>`_

  * - rstcheck
    -  * A pre-commit hook that runs when you commit and checks that your rST is
         valid.
    -  * `Repo <https://github.com/rstcheck/rstcheck>`_
       * `Docs <https://rstcheck.readthedocs.io/en/latest/>`_

  * - Sphinx
    -  * Static site generator.
       * Takes the source code + the table of contents + the theme to generate
         HTML output of the documentation. Often you can preview the
         documentation locally on your machine to verify it looks right.
       * Customized with the ``conf.py`` file.
    -  * `Sphinx docs (official) <https://www.sphinx-doc.org/en/master/#>`_
       * `Read the Docs <https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html>`_
         has many excellent guides on Sphinx.
       * Additionally, consider joining the
         `Write the Docs Slack Workspace <https://www.writethedocs.org/slack/>`_
         where you can talk to the maintainer of this theme and other
         users who use this theme on the ``#sphinx`` channel.

  * - Sphinx extensions
    -  * Sphinx extensions add additional functionality to both the Sphinx docs
         and to the Furo documentation theme.
       * The most important extension in the Salt Docs tech stack is
         `Sphinx Design <https://sphinx-design.readthedocs.io/en/furo-theme/>`_.
    -  * :ref:`sphinx-extensions`
       * :ref:`rst-guide`

  * - Venv and Pyenv
    -  * Virtual environments that can help you set up your local environment so
         that it has the correct dependencies to build the Salt docs.
    -  You're on your own. Good luck. :octicon:`heart-fill;1em;sd-text-danger`
