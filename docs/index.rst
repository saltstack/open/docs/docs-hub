:sd_hide_title:

==============
Salt docs hub
==============

.. include:: topics/_includes/overview.rst

.. toctree::
   :maxdepth: 2
   :hidden:

   topics/home
   topics/personas

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Salt docs tech stack

   topics/tech-stack
   topics/sphinx-extensions
   topics/furo-light-dark

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contributing to docs

   topics/contributing
   topics/working-group
   topics/rst-guide
   topics/style-guide
   Salt branding guide <https://gitlab.com/saltstack/open/salt-branding-guide>
   See a problem? Open an issue! <https://gitlab.com/groups/saltstack/open/docs/-/issues/>
   Docs hub repository <https://gitlab.com/saltstack/open/docs/docs-hub>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Policies and procedures

   topics/docs-triage-process
   topics/branch-versioning

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Works in progress

   topics/sample-repo-landing
   topics/sample-docs-landing
   topics/module-doc-standards/index
