Sphinx-Substitution-Extensions>=2020.9.30.0
furo>=2022.02.14.1
sphinx-copybutton>=0.5.0
sphinx-design
sphinx-inline-tabs>=2022.1.2b11
sphinx-prompt
sphinx-tabs>=3.3.1
sphinx>=4.4.0
