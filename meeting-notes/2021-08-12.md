# 2021-08-12 Meeting

## Attendees

- Derek Ardolf
- Wayne Werner
- Gary Giesen
- Bryce Larson
- Gareth Greenaway

## Updates

**Derek**

- Updated `salt-install-guide`
  - Recent Native minion releases
  - Example, using new `sphinx-design`: https://saltstack.gitlab.io/open/docs/salt-install-guide/topics/install/native/arista.html#download-and-verify-files
- Thinking of using `sphinx-design` for main landing page in future?
- Updated docs landing page for book link:
  - Old link: http://www.oreilly.com/webops-perf/free/network-automation-at-scale.csp
  - New link: https://www.cloudflare.com/network-automation-at-scale-ebook/


## What we worked on this meeting

**Derek**

- Reviewing Gary's MRs in `salt-user-guide`
- [`salt-extension`](https://github.com/saltstack/salt-extension) work

**Gary**

Looking into `vale` and the implementation of it
- `vale` user experience:
  - Is there potentially a custom git-hook that would download the latest `vale` after doing a clone of the repo (other than a git submodule)
  - What if it is in the `.pre-commit-config.yml`?
    - Run a `pre-commit run --all` or `pre-commit run latest-vale` (or whatever the pre-commit job is)?
  - Use the [`saltstack/open/salt-branding-guide`](https://gitlab.com/saltstack/open/salt-branding-guide) for `vale` style guide configs (in a `/vale/`)
  - [vale docs](https://docs.errata.ai/vale/about)

Making a directory at the root of `salt-branding-guide` for `style-guide`, to contain `vale` and other writing-guide-related tooling/docs.

- [`salt-extension`](https://github.com/saltstack/salt-extension) work

**Wayne**

- [`salt-extension`](https://github.com/saltstack/salt-extension) work

## Working group functionality

- Next meeting will be a "working" working group, where we use the time to work on tasks
  - Meetings, moving forward, are working meetups (at least until `salt-user-guide`
    and `salt-install-guide` are in MVP states and published)
- Paths for `docs.saltproject.io` publishing were logged in a previous working group for follow-up
  - [Meeting notes for 2021-07-08](https://gitlab.com/saltstack/open/docs/docs-hub/-/blob/main/meeting-notes/2021-07-08.md)

## Next steps

- Publish the `salt-user-guide` to `docs.saltproject.io/salt/user-guide/`
- Publish the `salt-install-guide` to `docs.saltproject.io/salt/install-guide/`
- See about implementing Algolia (or simply DuckDuckGo) for the current as-is docs portal

### Misc tasks

- Duplicate content from [VMware KB on Upgrading Salt](https://kb.vmware.com/s/article/50122319?lang=en_US&queryTerm=upgrading+your+salt+infrastructure) to install guide via [issue in `salt-install-guide`](https://gitlab.com/saltstack/open/docs/salt-user-guide/-/merge_requests/57)
  - Recent **Salt Air** mentioned that the `salt-master` doesn't need to be upgraded before minions
    - Upgrade documentation can say that we **recommend** upgrading the `salt-master` before upgrading minions.
- Add [Quick Guide to Vault Integration](https://web.archive.org/web/20210306232428/https://help.saltstack.com/hc/en-us/articles/360041140451-Quick-Guide-to-Vault-Integration) tutorial to `salt-user-guide`
- [Modify the main landing page of `salt-install-guide`](https://gitlab.com/saltstack/open/docs/salt-install-guide/-/issues/13)
- Confirm that Google tagging and analytics are stripped out where currently existing
- DuckDuckGo: Simple poc for docs landing `docs.saltproject.io`
