# 2022-mm-dd meeting

## Attendees

-

## Meeting agenda

### Welcome to newcomers!

If you are new to the group, tell us about yourself!


### Announcements

- The next Docs working group meeting is [date] at 3p.m. Mountain Time / 2p.m. Pacific Time / 22:00 UTC.


### Agenda item

-


### Agenda item

-


## Next docs working group meeting: [date]

Future:
-
